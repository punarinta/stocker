<?php

chdir(__DIR__);
system('clear');
date_default_timezone_set('Europe/Stockholm');

include_once 'lib/Stock.php';

$stocks = [];
$monitoredData = json_decode(file_get_contents('monitored.json'), true) ?:[];

// init stocks
foreach ($monitoredData as $monitoredItem)
{
    $stock = new Stock($monitoredItem['code']);
    $stock->initialPrice = $monitoredItem['price'];
    $stock->initialAmount = $monitoredItem['amount'];
    $stocks[] = $stock;
}

if ($argc > 1)
{
    switch ($argv[1])
    {
        case 'zero':
            $stock = Stock::findByCode($stocks, $argv[2]);
            echo ($stock ? $stock->getMinSellPrice(0) : 'no stock found');
            break;
    }

    echo "\n\n";

    return;
}

// Yahoo limit is 2000 req/hour/ip

// calculate sleep time
$sleepTime = min(2, (count($monitoredData) * 3600) / 2000);

while(1)
{
    foreach ($stocks as $stock)
    {
        $stock->sync();
        usleep($sleepTime * 1000000);

        $ts = date('H:i:s');
        $ts2 = date('H:i:s', $stock->onlineData['ts']);
        $currentPrice = number_format($stock->onlineData['price'], 2);

        echo "\n[{$ts}, {$ts2}] {$stock->code} = $currentPrice (" . number_format($stock->getProfit($currentPrice), 2) . "%) ";

        if ($currentPrice >= $stock->getMinSellPrice())
        {
            echo "\033[1;31m SELL \033[0m (min={$stock->getMinSellPrice()}, curr=$currentPrice) ";
        }
    }
}
