<?php

class Stock
{
    public $code = null;
    public $initialPrice = 0;
    public $initialAmount = 0;
    public $onlineData = null;

    public function __construct($code)
    {
        $this->code = strtr($code, [' ' => '-']);
    }

    public function sync()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, "http://finance.yahoo.com/webservice/v1/symbols/{$this->code}.ST/quote?format=json&view=detail");
        $result = curl_exec($ch);
        curl_close($ch);
        if ($data = json_decode($result, true))
        {
            $this->onlineData = $data['list']['resources'][0]['resource']['fields'];
        }
        else
        {
            // todo: parse error
        }
    }

    /**
     * Returns a minimum price of a stock to sell at to gain revenue with a desired interest rate
     *
     * @param int $interest     - interest rate in percents
     * @return float
     */
    public function getMinSellPrice($interest = 2)
    {
        $cost0 = $this->initialPrice * $this->initialAmount;
        $cost = ($cost0 + $this->getCourtage($cost0)) * (1 + $interest / 100);
        $sell = $cost + $this->getCourtage($cost);

        return $sell / $this->initialAmount;
    }

    /**
     * Returns a profit on a deal, includes courtages
     *
     * @param $sellPrice        - current single stock price
     * @return float
     */
    public function getProfit($sellPrice)
    {
        $cost0 = $this->initialPrice * $this->initialAmount;
        $cost = $cost0 + $this->getCourtage($cost0);
        $sell = $sellPrice * $this->initialAmount - $this->getCourtage($sellPrice * $this->initialAmount);

        return (($sell - $cost) / $cost) * 100;
    }

    public function getCourtage($price)
    {
        return $price < 400 ? 1 : 0.0025 * $price;
    }

    /**
     * Finds a stock in a heap by its code
     *
     * @param $stocks
     * @param $code
     * @return null
     */
    static public function findByCode($stocks, $code)
    {
        foreach ($stocks as $stock)
        {
            if ($stock->code == strtr(strtoupper($code), [' ' => '-']))
            {
                return $stock;
            }
        }

        return null;
    }
}
